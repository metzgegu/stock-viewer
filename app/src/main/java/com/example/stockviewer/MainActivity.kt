package com.example.stockviewer

import android.app.PendingIntent.getActivity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter
import com.jjoe64.graphview.series.LineGraphSeries

import com.jjoe64.graphview.series.DataPoint
import khttp.responses.Response
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList
import khttp.delete as httpDelete


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        getData()



    }

    fun getData() {

        Thread(){
            val response : Response =  khttp.get(
                url = "https://www.alphavantage.co/query",
                params = mapOf("function" to "TIME_SERIES_DAILY", "symbol" to "MSFT", "apikey" to "E0JZY3NWXFSRMO28"))
            val obj : JSONObject = response.jsonObject
            //println(obj.get("Time Series (Daily)"))
            val tab = obj.getJSONObject("Time Series (Daily)")


            //val keys = obj.getJSONObject("Time Series (Daily)").keys()
            val keys = ArrayList<String>();
            val seriesDataPoint = ArrayList<DataPoint>();
            obj.getJSONObject("Time Series (Daily)").keys().forEach { e ->
                keys.add(e);

            }
            for(i in 9 downTo 0 step 1) {
                val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
                val d1 = simpleDateFormat.parse(keys.get(i))
                seriesDataPoint.add(DataPoint(d1, tab.getJSONObject(keys.get(i)).getDouble("4. close")))
            }
            println("hello")
            val series = LineGraphSeries<DataPoint>(Array(10) {i -> seriesDataPoint.get(i) })
            graph.addSeries(series)

            //graph.getGridLabelRenderer().setLabelFormatter(DateAsXAxisLabelFormatter(this, SimpleDateFormat("dd") ));
            graph.getGridLabelRenderer().setLabelFormatter(DateAsXAxisLabelFormatter(this))
            graph.getGridLabelRenderer().setHorizontalLabelsAngle(90)
            graph.getGridLabelRenderer().labelHorizontalHeight = 300

            runOnUiThread({
                //Update UI
            })
        }.start()
    }
}
